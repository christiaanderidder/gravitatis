package nl.awesomestuff.gravitatis;

import nl.awesomestuff.gravitatis.objects.GameObject;

import org.andengine.input.touch.TouchEvent;

public interface IGameObjectTouchListener
{
	boolean onGameObjectTouched(GameObject go, TouchEvent event, float relativeX, float relativeY);
}
