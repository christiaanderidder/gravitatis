package nl.awesomestuff.gravitatis.editor;

import nl.awesomestuff.gravitatis.Constants;
import nl.awesomestuff.gravitatis.Helpers;
import nl.awesomestuff.gravitatis.objects.GameObject;
import nl.awesomestuff.gravitatis.resources.ResourceManager;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

public class Grid extends Entity
{
	private int mRows;
	private int mCols;
	private float mWidth;
	private float mHeight;
	private float mColSize;
	private float mRowSize;
	
	private Rectangle mSnappingIndicator;
	private float[] mPositionOnGrid;
	private float[] mPositionOnScene;
	
	public Grid(int cellSize, float width, float height)
	{
		VertexBufferObjectManager vbom = ResourceManager.getInstance().getEngine().getVertexBufferObjectManager();
		
		mWidth = width;
		mHeight = height;
		
		mColSize = cellSize;
		mRowSize = cellSize;
		
		mCols = (int) Math.ceil(mWidth / mColSize);
		mRows = (int) Math.ceil(mHeight / mRowSize);
		
		mSnappingIndicator = new Rectangle(0,0,0,0, vbom);
		mSnappingIndicator.setColor(new Color(1.f, 0, 0));
		mSnappingIndicator.setAlpha(0.3f);
		
		createGridLines();
		attachChild(mSnappingIndicator);
	}
	
	private void createGridLines()
	{
		Color lineColor = Color.WHITE;
		
		lineColor.setAlpha(0.5f);
		
		float mainLineWidth = 3.f;
		float mainLineAlpha = 0.5f;
		
		float subLineWidth = 1.f;
		float subLineAlpha = 0.3f;
		
		lineColor.setAlpha(mainLineAlpha);
		
		ResourceManager res = ResourceManager.getInstance();
		
		for(int i = 0; i < mRows; i++)
		{
			Line l = new Line(0, i * mRowSize, mWidth, i * mRowSize, 0, res.getEngine().getVertexBufferObjectManager());
			
			l.setColor(lineColor);
			
			if(i % 4 == 0)
			{
				l.setLineWidth(mainLineWidth);
				l.setAlpha(mainLineAlpha);
			}
			else
			{
				l.setLineWidth(subLineWidth);
				l.setAlpha(subLineAlpha);
			}
			
			attachChild(l);
		}
		
		for(int i = 0; i < mCols; i++)
		{
			Line l = new Line(i * mColSize, 0, i * mColSize, mHeight, 0, res.getEngine().getVertexBufferObjectManager());
			
			l.setColor(lineColor);
			
			if(i % 4 == 0)
			{
				l.setLineWidth(mainLineWidth);
				l.setAlpha(mainLineAlpha);
			}
			else
			{
				l.setLineWidth(subLineWidth);
				l.setAlpha(subLineAlpha);
			}
			
			attachChild(l);
		}
	}
	
	public float snapXToGrid(float x)
	{
		float col = Math.round(x / mColSize);
		return col * mColSize;
	}
	
	public float snapYToGrid(float y)
	{
		float row = Math.round(y / mRowSize);
		return row * mRowSize;
	}

	public void showSnappingIndicator(GameObject go)
	{
		mPositionOnGrid = convertParentCoordinatesToLocalCoordinates(go.getX() - (go.getWidth() / 2.f), go.getY() - (go.getHeight() / 2.f));
		mPositionOnGrid[0] = snapXToGrid(Helpers.clamp(mPositionOnGrid[0], 0, mWidth - go.getWidth())) + (go.getWidth() / 2.f);
		mPositionOnGrid[1] = snapYToGrid(Helpers.clamp(mPositionOnGrid[1], 0, mHeight - go.getHeight())) + (go.getHeight() / 2.f);
		
		mSnappingIndicator.setSize(go.getWidth(), go.getHeight());
		mSnappingIndicator.setRotation(go.getRotation());
		
		mSnappingIndicator.setPosition(mPositionOnGrid[0], mPositionOnGrid[1]);
		mSnappingIndicator.setVisible(true);
	}
	
	public void hideSnappingIndicator()
	{
		mSnappingIndicator.setVisible(false);
	}

	public void snapObjectToGrid(GameObject go)
	{
		mPositionOnGrid = convertParentCoordinatesToLocalCoordinates(go.getX() - (go.getWidth() / 2.f), go.getY() - (go.getHeight() / 2.f));
		mPositionOnGrid[0] = snapXToGrid(Helpers.clamp(mPositionOnGrid[0], 0, mWidth - go.getWidth())) + (go.getWidth() / 2.f);
		mPositionOnGrid[1] = snapYToGrid(Helpers.clamp(mPositionOnGrid[1], 0, mHeight - go.getHeight())) + (go.getHeight() / 2.f);
		
		mPositionOnScene = convertLocalCoordinatesToParentCoordinates(mPositionOnGrid[0], mPositionOnGrid[1]);
		
		//go.setPosition(mPositionOnScene[0], mPositionOnScene[1]);
		go.getBody().setTransform(mPositionOnScene[0] / Constants.PHYSICS_PPM, mPositionOnScene[1] / Constants.PHYSICS_PPM, go.getBody().getAngle());
		
		mSnappingIndicator.setVisible(false);
	}
}
