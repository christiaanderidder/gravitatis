package nl.awesomestuff.gravitatis;

import javax.microedition.khronos.opengles.GL10;

import nl.awesomestuff.gravitatis.resources.ResourceManager;
import nl.awesomestuff.gravitatis.scenes.ManagedScene;
import nl.awesomestuff.gravitatis.scenes.SceneManager;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.modifier.IModifier;


public class ShellScreen extends ManagedScene implements IEntityModifierListener 
{
	private Rectangle mSplash;
	private ManagedScene mNextScene;
	private float mTimeOut;
	private String mContent;
	private Text mText;
	
	private AlphaModifier mFadeIn;
	private DelayModifier mWait;
	private AlphaModifier mFadeOut;
	
	public ShellScreen(ManagedScene nextScene, String content)
	{
		this(nextScene, 0, content);
	}
	
	public ShellScreen(ManagedScene nextScene, float timeOut, String content)
	{
		mNextScene = nextScene;
		mContent = content;
		mTimeOut = timeOut;
	}
	
	@Override
	public void onLoad() {
		// TODO Auto-generated method stub
		ResourceManager res = ResourceManager.getInstance();

		
		mSplash = new Rectangle(res.getCameraWidth()/2.f, res.getCameraHeight()/2.f, res.getCameraWidth(), res.getCameraHeight(), res.getEngine().getVertexBufferObjectManager());
		mSplash.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mSplash.setColor(Constants.COLOR_WHITE);
		mSplash.setAlpha(0);
		
		attachChild(mSplash);
		

		mFadeIn = new AlphaModifier(0.2f, 0, 1, this);
		mWait = new DelayModifier(mTimeOut, this);
		mFadeOut= new AlphaModifier(0.2f, 1, 0, this);
		
		mSplash.registerEntityModifier(new SequenceEntityModifier(mFadeIn, mWait, mFadeOut));
		
		
		mText = new Text(res.getCameraWidth()/2.f, res.getCameraHeight()/2.f, res.getFonts().text, mContent, new TextOptions(HorizontalAlign.CENTER), res.getEngine().getVertexBufferObjectManager());
		mText.setVisible(false);
		mText.setColor(Constants.LEVEL_BACKGROUND_COLOR);
		
		attachChild(mText);
	}

	@Override
	public void onModifierStarted(IModifier<IEntity> modifier, IEntity item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onModifierFinished(IModifier<IEntity> modifier, IEntity item) {
		
		if(modifier == mFadeIn)
		{
			mText.setVisible(true);
		}
		else if(modifier == mWait)
		{
			mText.setVisible(false);
		}
		else if(modifier == mFadeOut)
		{
			SceneManager.getInstance().showScene(mNextScene);
		}
		
		
	}
	
}
