package nl.awesomestuff.gravitatis;

public interface ILaunchListener {
	void onLaunched();
}
