package nl.awesomestuff.gravitatis.managers;

public class GameManager
{
	private static GameManager INSTANCE;

	private static final int INITIAL_SCORE = 0;
	private static final int INITIAL_LEVEL = 0;

	private int mCurrentScore;
	private int mCurrentLevel;

	private GameManager()
	{
	}

	public static GameManager getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new GameManager();
		}
		return INSTANCE;
	}

	public int getCurrentScore()
	{
		return this.mCurrentScore;
	}

	public int getCurrentLevel()
	{
		return this.mCurrentLevel;
	}

	public void incrementScore(int pIncrementBy)
	{
		mCurrentScore += pIncrementBy;
	}

	public void incrementLevel(int pIncrementBy)
	{
		mCurrentLevel += pIncrementBy;
	}

	public void resetGame()
	{
		this.mCurrentScore = GameManager.INITIAL_SCORE;
		this.mCurrentLevel = GameManager.INITIAL_LEVEL;
	}
}
