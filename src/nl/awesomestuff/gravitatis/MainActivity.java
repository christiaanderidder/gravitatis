package nl.awesomestuff.gravitatis;

import nl.awesomestuff.gravitatis.resources.ResourceManager;
import nl.awesomestuff.gravitatis.scenes.SceneManager;

import org.andengine.audio.music.Music;
import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.ZoomCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.ui.activity.BaseGameActivity;

import android.util.Log;
import android.view.KeyEvent;

import com.badlogic.gdx.math.Vector2;

public class MainActivity extends BaseGameActivity
{
	//private static final String TAG = "MainActivity";
		
	private static final float SCREEN_WIDTH = 1280f;
	private static final float SCREEN_HEIGHT = 720f;
	
	/*private static final float MIN_ZOOM_FACTOR = 1.f;
	private static final float MAX_ZOOM_FACTOR = 5f;

	private float mInitialTouchZoomFactor;
	
	
	private PinchZoomDetector mPinchZoomDetector;*/
	private ZoomCamera mCamera;
	private PhysicsWorld mPhysicsWorld;
	private boolean mDevModeEnabled;

	
	/*
	 * The onCreateEngineOptions method is responsible for creating the options
	 * to be applied to the Engine object once it is created. The options
	 * include, but are not limited to enabling/disable sounds and music,
	 * defining multitouch options, changing rendering options and more.
	 */
	@Override
	public EngineOptions onCreateEngineOptions()
	{
		mDevModeEnabled = false;
		mCamera = new ZoomCamera(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		mPhysicsWorld = new FixedStepPhysicsWorld(Constants.PHYSICS_STEPS_SECOND,
				new Vector2(0, -Constants.PHYSICS_GRAVITY),
				false, Constants.PHYSICS_VELOCITY_ITERATIONS,
				Constants.PHYSICS_POSITION_ITERATIONS);
		
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), mCamera);
		
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		engineOptions.getRenderOptions().setDithering(true);	
		engineOptions.getRenderOptions().getConfigChooserOptions().setRequestedMultiSampling(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		
		return engineOptions;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{  
		SceneManager sm = SceneManager.getInstance();
	    if (keyCode == KeyEvent.KEYCODE_MENU)
	    {
	    	mDevModeEnabled = !mDevModeEnabled;
	    	Log.i("Gravitatis", "DEV MODE: " + mDevModeEnabled);
	        sm.onDevModeToggled(mDevModeEnabled);
	    	return true; 
	    }
	    else if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
	    	sm.onBackPressed();
	    	return true; 
	    }
	    return false; 
	}
	
	@Override
	public Engine onCreateEngine(final EngineOptions pEngineOptions)
	{
		return new FixedStepEngine(pEngineOptions, 60);
	}

	/*
	 * The onCreateResources method is in place for resource loading, including
	 * textures, sounds, and fonts for the most part.
	 */
	@Override
	public void onCreateResources(OnCreateResourcesCallback callback)
	{
		ResourceManager.getInstance().setup(this.getEngine(), this.getApplicationContext(), mCamera, mPhysicsWorld);
		
		ResourceManager.getInstance().loadResources();
		
		// The callback to notify the engine we have finished loading.
		callback.onCreateResourcesFinished();
	}

	/*
	 * The onCreateScene method is in place to handle the scene initialization
	 * and setup. In this method, we must at least *return our mScene object*
	 * which will then be set as our main scene within our Engine object
	 * (handled "behind the scenes"). This method might also setup touch
	 * listeners, update handlers, or more events directly related to the scene.
	 */
	@Override
	public void onCreateScene(OnCreateSceneCallback callback)
	{
		SceneManager sm = SceneManager.getInstance();
		sm.showScene(new SplashScreen());		
		callback.onCreateSceneFinished(sm.getCurrentScene());

	}

	/*
	 * The onPopulateScene method was introduced to AndEngine as a way of
	 * separating scene-creation from scene population. This method is in place
	 * for attaching child entities to the scene once it has already been
	 * returned to the engine and set as our main scene.
	 */
	@Override
	public void onPopulateScene(Scene scene, OnPopulateSceneCallback onPopulateSceneCallback)
	{
		// The callback to notify the engine we have finished populating the scene.
		onPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	/*
	 * Music objects which loop continuously should be played in onResumeGame()
	 * of the activity life cycle
	 */
	@Override
	public synchronized void onResumeGame()
	{
		Music m = ResourceManager.getInstance().getMusic().menu;
		if (m != null && !m.isReleased() && !m.isPlaying())
		{
			m.setLooping(true);
			m.play();
		}
		super.onResumeGame();
	}

	/*
	 * Music objects which loop continuously should be paused in onPauseGame()
	 * of the activity life cycle
	 */
	@Override
	public synchronized void onPauseGame()
	{
		Music m = ResourceManager.getInstance().getMusic().menu;
		if (m != null && !m.isReleased() && m.isPlaying())
		{
			m.pause();
		}
	}

	
	@Override
	public void onBackPressed()
	{
		SceneManager.getInstance().onBackPressed();
	}

	/*@Override
	public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent)
	{
		mPinchZoomDetector.onTouchEvent(touchEvent);
		
		if(touchEvent.isActionMove())
		{
			//mPlayer.setPosition(touchEvent.getX(), touchEvent.getY());
			return true;
		}
		return false;
	}

	@Override
	public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector, TouchEvent pSceneTouchEvent)
	{
		mInitialTouchZoomFactor = mCamera.getZoomFactor();
	}

	@Override
	public void onPinchZoom(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor)
	{
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;
		// If the camera is within zooming bounds
		if(newZoomFactor < MAX_ZOOM_FACTOR && newZoomFactor > MIN_ZOOM_FACTOR)
		{
			// Set the new zoom factor
			mCamera.setZoomFactor(newZoomFactor);
		}
	}

	@Override
	public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor)
	{
		// TODO Auto-generated method stub
		
	}*/

}
