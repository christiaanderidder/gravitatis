package nl.awesomestuff.gravitatis;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
	 
public class DBOpenHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "Gravitatis.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String TABLE = "highscores";
	private static final String COLUMN_ID = "_id";
	private static final String COLUMN_LEVEL = "level";
	private static final String COLUMN_SCORE = "score";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String[] COLUMNS = new String[]{COLUMN_ID, COLUMN_LEVEL, COLUMN_SCORE, COLUMN_TIMESTAMP};

	public DBOpenHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL("CREATE TABLE IF NOT EXISTS "+ TABLE+ " (" +
            COLUMN_ID 		+ " INTEGER PRIMARY KEY, " +
            COLUMN_LEVEL	+ " TEXT NOT NULL, " +	
            COLUMN_SCORE 	+ " INTEGER NOT NULL, " +
            COLUMN_TIMESTAMP+ " INTEGER NOT NULL " +
            ")");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE);
		onCreate(db);
	}
	
	public Highscore getHighScoreForLevel(String levelName)
    {         	 
		Highscore h = null;
		SQLiteDatabase db = this.getReadableDatabase();
   	 
        Cursor c = db.query(TABLE, COLUMNS, COLUMN_LEVEL + "='" + levelName + "'", null, null, null, COLUMN_SCORE + " DESC", " 1");
        c.moveToFirst();
        while(!c.isAfterLast())
        {
	       	 h = new Highscore(c.getString(c.getColumnIndex(COLUMN_LEVEL)));
	       	 h.setId(c.getLong(c.getColumnIndex(COLUMN_ID)));
	       	 h.setScore(c.getLong(c.getColumnIndex(COLUMN_SCORE)));
	       	 h.setTimestamp(c.getLong(c.getColumnIndex(COLUMN_TIMESTAMP)));
	       	 c.moveToNext();
        }

        c.close();
        
        return h;
    }
   
     public ArrayList<Highscore> getHighScoresForLevel(String levelName)
     {         	 
    	 ArrayList<Highscore> highscores = new ArrayList<Highscore>();
    	 SQLiteDatabase db = this.getReadableDatabase();
    	 
         Cursor c = db.query(TABLE, COLUMNS, COLUMN_LEVEL + "='" + levelName + "'", null, null, null, COLUMN_SCORE + " DESC");
         c.moveToFirst();
         while(!c.isAfterLast())
         {
        	 Highscore h = new Highscore(c.getString(c.getColumnIndex(COLUMN_LEVEL)));
        	 h.setId(c.getLong(c.getColumnIndex(COLUMN_ID)));
        	 h.setScore(c.getLong(c.getColumnIndex(COLUMN_SCORE)));
        	 h.setTimestamp(c.getLong(c.getColumnIndex(COLUMN_TIMESTAMP)));
        	 highscores.add(h);
        	 c.moveToNext();
         }

         c.close();
         
         return highscores;
     }
     
     public void addHighScore(Highscore highscore)
     {         
             SQLiteDatabase db = this.getWritableDatabase();
             ContentValues cv = new ContentValues();
             
             cv.put(COLUMN_LEVEL, highscore.getLevel());
             cv.put(COLUMN_SCORE, highscore.getScore());
             cv.put(COLUMN_TIMESTAMP, highscore.getTimestamp());
             
             db.insert(TABLE, null, cv);
     }

	 
}
