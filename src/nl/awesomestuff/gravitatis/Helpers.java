package nl.awesomestuff.gravitatis;

public class Helpers
{
	public static float clamp (float i, float low, float high)
	{
		return java.lang.Math.max (java.lang.Math.min (i, high), low);
	}
	
	public static float positionToDegrees(float x, float y)
	{
		float degrees = (float)(Math.atan2(x, y) * (180 / Math.PI));
		//return degrees;
		return (degrees > 0) ? degrees : degrees + 360.f;
	}
}
