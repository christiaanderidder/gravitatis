package nl.awesomestuff.gravitatis;
import java.util.ArrayList;
import java.util.Iterator;

import nl.awesomestuff.gravitatis.editor.Grid;
import nl.awesomestuff.gravitatis.objects.GameObject;
import nl.awesomestuff.gravitatis.objects.LaunchPad;
import nl.awesomestuff.gravitatis.objects.ObjectFactory;
import nl.awesomestuff.gravitatis.resources.ResourceManager;
import nl.awesomestuff.gravitatis.scenes.ManagedScene;
import nl.awesomestuff.gravitatis.scenes.SceneManager;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.Manifold;


public class GameScene extends ManagedScene implements IGameObjectTouchListener, ILaunchListener, ContactListener
{
	
	private static final int STATE_PLAYING = 0;
	private static final int STATE_EDITING = 1;
	private static final int STATE_FINISHED = 2;
	private int mState;
	
	private Highscore mScore;
	private Highscore mHighScore;
	
	private Level mLevel;
	private Grid mGrid;
	private Inventory mInventory;
	private Text mScoreText;
	private LaunchPad mLaunchPad;
	private GameObject mPlayer;
	private PhysicsWorld mPhysicsWorld;
	private DebugRenderer mDebugRenderer;
	private String mNewLevel;
	
	private ArrayList<GameObject> mArrows;
	
	private float mTick;
	private long mTimeStarted;
	
	float mInitialOffsetX;
	float mInitialOffsetY;
	
	float mPlayableAreaWidth;
	float mPlayableAreaHeight;
	
	private Text mDevModeText;
	private boolean mIsDevMode;
	
	public GameScene(Level level)
	{
		mLevel = level;
		mState = STATE_EDITING;
	}
	
	@Override
	public void onLoad()
	{	
		ResourceManager res = ResourceManager.getInstance();
		mNewLevel = null;
		mArrows = new ArrayList<GameObject>();
		mIsDevMode = false;
		mScore = new Highscore(mLevel.getFileName());
		
		mPlayableAreaWidth = res.getCameraWidth() - Constants.SIDEBAR_WIDTH;
		mPlayableAreaHeight = res.getCameraHeight();
		
		VertexBufferObjectManager vbom = res.getEngine().getVertexBufferObjectManager();
		
		mPhysicsWorld = res.getPhysicsWorld();
		mPhysicsWorld.setContactListener(this);
		
		registerUpdateHandler(mPhysicsWorld);
		
		
		
		mGrid = new Grid(Constants.LEVEL_GRID_CELL_SIZE, mPlayableAreaWidth, mPlayableAreaHeight);
		mInventory = new Inventory(mLevel, res.getTextures().inventory, mPlayableAreaWidth + Constants.SIDEBAR_WIDTH / 2.f, mPlayableAreaHeight / 2.f, Constants.SIDEBAR_WIDTH,	mPlayableAreaHeight, vbom);
		
		mHighScore = res.getHighScoreForLevel(mLevel.getFileName());

		mScoreText = new Text(mPlayableAreaWidth + (Constants.SIDEBAR_WIDTH /2.f), mPlayableAreaHeight-60.f, res.getFonts().textSmall, "Your\nScore\n      ", new TextOptions(HorizontalAlign.CENTER), vbom);
		
		if(mLevel.getName().equals(Constants.MAIN_MENU_NAME))
		{
			mScoreText.setText("Main\nMenu");
		}
		else
		{
			mScoreText.setText("Best\nScore\n"+ ((mHighScore != null ) ? mHighScore.getScore() : 0));
		}
		
		mScoreText.setAlpha(1.f);
		mDevModeText = new Text(mPlayableAreaWidth/2.f, mPlayableAreaHeight/2.f, res.getFonts().text, "DEVMODE", new TextOptions(HorizontalAlign.CENTER), vbom);
		mDevModeText.setColor(new Color(1.f, 0, 0));
		mDevModeText.setVisible(false);
		
		mPlayer = ObjectFactory.create("player", -50, -50, 64, 64, 0);
		mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(mPlayer, mPlayer.getBody(), true, true));
		mPlayer.setVisible(false);
		mLaunchPad = new LaunchPad(mPlayableAreaWidth / 2.f, mPlayableAreaHeight / 2.f, mPlayer, this, vbom);
		
		setBackground(new Background(Constants.LEVEL_BACKGROUND_COLOR));
				
		attachChild(mGrid);
		attachChild(mDevModeText);
		
		if(mLevel.getName().equals(Constants.MAIN_MENU_NAME))
		{
			Sprite guide = new Sprite(mPlayableAreaWidth / 2.f,  mPlayableAreaHeight / 2.f, mPlayableAreaWidth, mPlayableAreaHeight, ResourceManager.getInstance().getTextures().menu, vbom);
			guide.setAlpha(1.0f);
			attachChild(guide);
		}
		
		attachChild(mInventory);
		registerTouchArea(mInventory);
		
		attachChild(mScoreText);
		
		createWalls();
		createArrows();
		createGameObjects();
		
		attachChild(mLaunchPad);
		registerTouchArea(mLaunchPad);
		
		attachChild(mPlayer);
		
		mLaunchPad.setZIndex(200);
		mPlayer.setZIndex(201);
		
		/*mButton = new ButtonSprite(256, 64, ResourceManager.getInstance().getTextures().menuLevelSelect, vbom)
		{
			@Override
			public boolean onAreaTouched(TouchEvent pTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY)
			{
				if(pTouchEvent.isActionDown())
				{
					SceneManager.getInstance().showScene(new GameScene(mLevel));
				}
				return super.onAreaTouched(pTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		
		attachChild(mButton);
		registerTouchArea(mButton);*/
		
		mDebugRenderer = new DebugRenderer(mPhysicsWorld, vbom);
		mDebugRenderer.setVisible(false);
		attachChild(mDebugRenderer);
		
		setTouchAreaBindingOnActionDownEnabled(true);
		setTouchAreaBindingOnActionMoveEnabled(true);
	}
	
	private void createWalls()
	{
		float cw = mPlayableAreaWidth;
		float ch = mPlayableAreaHeight;
		
		float h = 1.f;
		float w = 1.f;
		
		attachChild(ObjectFactory.create("wall", (cw / 2.f), ch - (w / 2.f), cw, h, 0));
		attachChild(ObjectFactory.create("wall", (cw / 2.f), (h / 2.f), cw, h, 0));
		attachChild(ObjectFactory.create("wall", w/2.f, (ch /2.f), w, ch, 0));
		attachChild(ObjectFactory.create("wall", cw - (w / 2.f), (ch /2.f), w, ch, 0));
	}
	
	private void createArrows()
	{
		float count = 5;
		float step = mPlayableAreaWidth / count;
		for(int i = 0; i < count; i++)
		{
			GameObject go = ObjectFactory.create("gravity_arrow", (step * i) + 80.f, i%2==0?500:200, 0, 0, 180);
			mArrows.add(go);
			attachChild(go);
		}
		
	}
	
	private void createGameObjects()
	{
		for(GameObject go : mLevel.getFixedObjects())
		{
			attachChild(go);
			if(go.getType().equals("score_orb"))
			{
				mScore.addOrb();
			}
			mPhysicsWorld.registerPhysicsConnector(go.getPhysicsConnector());
		}
		
		for(GameObject go : mLevel.getPlacedObjects())
		{		
			attachChild(go);
			registerTouchArea(go);
			go.setTouchListener(this);
			mPhysicsWorld.registerPhysicsConnector(go.getPhysicsConnector());
		}
		
		for(GameObject go : mLevel.getInventoryObjects())
		{		
			attachChild(go);
			registerTouchArea(go);
			go.setTouchListener(mInventory);
			mPhysicsWorld.registerPhysicsConnector(go.getPhysicsConnector());
		}
	}

	@Override
	public boolean onGameObjectTouched(GameObject go, TouchEvent event, float relativeX, float relativeY)
	{
		if(mState == STATE_EDITING)
		{
			switch (event.getAction())
			{	
			 	case TouchEvent.ACTION_DOWN:
			 		mInitialOffsetX = event.getX() - go.getX();
			 		mInitialOffsetY = event.getY() - go.getY();
				    break;
				case TouchEvent.ACTION_MOVE:			
					go.getBody().setTransform((event.getX() - mInitialOffsetX) / Constants.PHYSICS_PPM, (event.getY() - mInitialOffsetY) / Constants.PHYSICS_PPM, go.getBody().getAngle());
					if(event.getX() > mPlayableAreaWidth)
					{
						go.getDrawable().setColor(Constants.COLOR_RED);
						mGrid.hideSnappingIndicator();
					}
					else if(!mIsDevMode)
					{
						mGrid.showSnappingIndicator(go);
					}
					go.getDrawable().setColor(Constants.COLOR_WHITE);
					
				    break;
				case TouchEvent.ACTION_UP:
				case TouchEvent.ACTION_CANCEL:
					if(event.getX() > mPlayableAreaWidth)
					{
						mInventory.addObject(go);
					}
					else if(!mIsDevMode)
					{
						mGrid.snapObjectToGrid(go);
					}
					break;			
			}
			return true;
		}
		return false;
	}

	@Override
	public void onBackPressed()
	{
		switch(mState)
		{
			case STATE_EDITING:
				//SceneManager.getInstance().showScene(new GameScene("menu"));
				//Level.saveToJson(mLevel);
				break;
			case STATE_PLAYING:
				mState = STATE_EDITING;
				mPhysicsWorld.setGravity(new Vector2(0, -Constants.PHYSICS_GRAVITY));
				mLaunchPad.resetPlayer();
				for(GameObject go : mLevel.getFixedObjects())
        		{
        			if(go.getType().equals("score_orb"))
        			{
        				go.setVisible(true);
        			}
        		}
				for(GameObject arrow : mArrows)
        		{
        			arrow.setRotation(180);
        		}
				break;
		}

		
	}
	
	@Override
	public void onDevModeToggled(boolean devMode)
	{
		mDevModeText.setVisible(devMode);
		mDebugRenderer.setVisible(devMode);
		mIsDevMode = devMode;
		for(GameObject go : mLevel.getFixedObjects())
		{
			if(devMode)
			{
				registerTouchArea(go);
				go.setTouchListener(this);
			}
			else
			{
				unregisterTouchArea(go);
				go.setTouchListener(null);			
			}
		}
	}
	
	@Override
	public void onManagedUpdate(float delta)
	{
		if(mNewLevel != null)
		{
			Level.saveToJson(mLevel);
			cleanUp();
			Level l = Level.loadFromJson(mNewLevel);
			SceneManager.getInstance().showScene(new ShellScreen(new GameScene(l), 2, l.getName()));
			
		}
		if(mState == STATE_FINISHED)
		{
			Level.saveToJson(mLevel);
			ResourceManager.getInstance().addHighScore(mScore);
			
			long best = mScore.getScore();
			if(mHighScore != null && mHighScore.getScore() > mScore.getScore())
				best = mHighScore.getScore();
			long timeNeeded = (mScore.getScore() > Constants.SCORE_INCREASE_LEVEL) ? ((mTimeStarted - System.currentTimeMillis()) / 1000) : 0;
			int orbscore = (mScore.getOrbCollectedCount() * Constants.SCORE_INCREASE_ORB);
			cleanUp();
			String message = "You completed " + mLevel.getName() + " = +" + Constants.SCORE_INCREASE_LEVEL + "\n"+
							 "You collected " + mScore.getOrbCollectedCount() + "/" + mScore.getOrbCount() + " Orbs = +" + orbscore +"\n" +
							 "Time penalty = -"  + (Constants.SCORE_INCREASE_LEVEL + orbscore - mScore.getScore())+"\n\n" + 
							 "Your score: " + mScore.getScore()  + "\n"  +
							 "Best score: " + best + "\n";
			SceneManager.getInstance().showScene(new ShellScreen(new GameScene(Level.loadFromJson(Constants.MAIN_MENU_NAME)),10, message));
			
		}
		else
		{
			mState = (mLaunchPad.hasLaunched()) ? STATE_PLAYING : STATE_EDITING;
	
			if(mState == STATE_PLAYING)
			{
				mInventory.setActive(false);
				
				if(mScore.getScore() > 0)
				{
					mTick += delta;
					
					if(mTick >= 0.1f)
					{
						mScore.decreaseScore(Constants.SCORE_DECREASE_SEC / 10);
						mTick = 0;
					}
				}
				
				if(!(mLevel.getName().equals(Constants.MAIN_MENU_NAME)))
				{
					mScoreText.setText("Score\n" + mScore.getScore());
				}
				
				
				Vector2 v = mPlayer.getBody().getLinearVelocity();
				if(Math.abs(v.x) <= 0.1f && Math.abs(v.y) <= 0.1f)
				{
					registerUpdateHandler(new TimerHandler(2f, new ITimerCallback()
					{
			            @Override
			            public void onTimePassed(TimerHandler pTimerHandler)
			            {
			            	Vector2 vlater = mPlayer.getBody().getLinearVelocity();
				            if (Math.abs(vlater.x) <= 0.1f && Math.abs(vlater.y) <= 0.1f)
				            {
				            	mPhysicsWorld.setGravity(new Vector2(0, -Constants.PHYSICS_GRAVITY));
				            	mLaunchPad.resetPlayer();    
				            	for(GameObject go : mLevel.getFixedObjects())
				        		{
				        			if(go.getType().equals("score_orb"))
				        			{
				        				go.setVisible(true);
				        			}
				        		}
				            	for(GameObject arrow : mArrows)
				        		{
				        			arrow.setRotation(180);
				        		}
				            }
			            }
					}));
				}
				
				for(GameObject go : mLevel.getFixedObjects())
				{
					go.getDrawable().setAlpha(1.f);
					go.getDrawable().setColor(Constants.COLOR_WHITE);
				}
			}
			else if(mState == STATE_EDITING)
			{
				mInventory.setActive(true);
				
				for(GameObject go : mLevel.getFixedObjects())
				{
					//go.getDrawable().setAlpha(0.8f);
					//go.getDrawable().setColor(Constants.COLOR_RED);
				}
			}
		}
		super.onManagedUpdate(delta);
	}
	
	@Override
	public void beginContact(Contact contact)
	{
		
		if(mState == STATE_PLAYING)
		{
			GameObject objA = (GameObject) contact.getFixtureA().getBody().getUserData();
			GameObject objB = (GameObject) contact.getFixtureB().getBody().getUserData();	
			
			if(objA == mPlayer && objB.getType().equals("gravity_modifier"))
			{
				setGravity(objB);
			}
			else if(objB == mPlayer && objA.getType().equals("gravity_modifier"))
			{
				setGravity(objA);
			}
			
			if(objA == mPlayer && objB.getType().contains("loader"))
			{
				loadLevel(objB);
			}
			else if(objB == mPlayer && objA.getType().contains("loader"))
			{
				loadLevel(objA);
			}
			
			if(objA == mPlayer && objB.getType().equals("score_orb") || objB == mPlayer && objA.getType().equals("score_orb"))
			{
				
				
			}
			
			if(objA == mPlayer && objB.getType().equals("score_orb"))
			{
				collectOrb(objB);
			}
			else if(objB == mPlayer && objA.getType().equals("score_orb"))
			{
				collectOrb(objA);
			}
			
			if(objA == mPlayer && objB.getType().equals("exit") || objB == mPlayer && objA.getType().equals("exit"))
			{
				mScore.increaseScore(Constants.SCORE_INCREASE_LEVEL);
				mState = STATE_FINISHED;
			}
		}
		
	}
	
	private void collectOrb(GameObject orb)
	{
		if(orb.isVisible())
		{
			orb.setVisible(false);
			mScore.addOrbCollected();
			mScore.increaseScore(Constants.SCORE_INCREASE_ORB);
		}
	}
	
	private void loadLevel(GameObject loader)
	{
		
		String[] level = loader.getType().split("-");
		mNewLevel = level[1];
		
	}
	
	private void setGravity(GameObject gravityModfier)
	{
		double radians = -gravityModfier.getRotation() * (Math.PI / 180.f);
		float facX = (float)Math.sin(radians);
		float facY = (float)Math.cos(radians);
		Log.i("Gravity Change", "rot: " + gravityModfier.getRotation() + " facX: " + facX + " facY: " +facY);
		mPhysicsWorld.setGravity(Vector2Pool.obtain(-Constants.PHYSICS_GRAVITY * facX, Constants.PHYSICS_GRAVITY * facY));
		
		for(GameObject arrow : mArrows)
		{
			arrow.setRotation(gravityModfier.getRotation());
		}
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}
	
	private void cleanUp()
	{
		Log.d("CleanUp", "cleaning");
		PhysicsWorld pw = ResourceManager.getInstance().getPhysicsWorld();
		pw.setGravity(new Vector2(0, -Constants.PHYSICS_GRAVITY));
		Iterator<Body> allMyBodies = pw.getBodies();
		while (allMyBodies.hasNext())
		{
			try
			{
				final Body myCurrentBody = allMyBodies.next();
				pw.destroyBody(myCurrentBody);
			}
			catch (Exception e)
			{
				Log.d("CleanUp", "THE BODY DOES NOT WANT TO DIE: " + e);
			}
		}

		Iterator<Joint> allMyJoints = pw.getJoints();
		while (allMyJoints.hasNext())
		{
			try 
			{
				final Joint myCurrentJoint = allMyJoints.next();
				pw.destroyJoint(myCurrentJoint);

			} catch (Exception e) {
				Log.d("CleanUp", "THE JOINT DOES NOT WANT TO DIE: " + e);
			}
		}
		
		pw.clearPhysicsConnectors();

		detachChildren();
		System.gc();
	}

	@Override
	public void onLaunched()
	{
		mTimeStarted = System.currentTimeMillis();
		mTick = 0;
		mScore.reset();
		
	}
}
