package nl.awesomestuff.gravitatis;

import javax.microedition.khronos.opengles.GL10;

import nl.awesomestuff.gravitatis.editor.Grid;
import nl.awesomestuff.gravitatis.resources.ResourceManager;
import nl.awesomestuff.gravitatis.scenes.ManagedScene;
import nl.awesomestuff.gravitatis.scenes.SceneManager;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.modifier.IModifier;


public class SplashScreen extends ManagedScene implements IEntityModifierListener 
{
	private Sprite mTitle;
	private Sprite mGuide;
	private Grid mGrid;
	
	
	private AlphaModifier mFadeIn;
	private DelayModifier mWait;
	private AlphaModifier mTitleFadeOut;
	private AlphaModifier mGuideFadeOut;
	

	
	@Override
	public void onLoad() {
		// TODO Auto-generated method stub
		setBackground(new Background(Constants.LEVEL_BACKGROUND_COLOR));
		
		ResourceManager res = ResourceManager.getInstance();

		mGrid = new Grid(Constants.LEVEL_GRID_CELL_SIZE, res.getCameraWidth(), res.getCameraHeight());
		
		mTitle = new Sprite(res.getCameraWidth()/2.f, res.getCameraHeight()/2.f, res.getTextures().title, res.getEngine().getVertexBufferObjectManager());
		mTitle.setAlpha(1);
		
		
		mGuide = new Sprite(res.getCameraWidth()/2.f, res.getCameraHeight()/2.f, res.getCameraWidth(), res.getCameraHeight(), res.getTextures().guide, res.getEngine().getVertexBufferObjectManager());
		mGuide.setAlpha(0);
		
		
		attachChild(mGrid);
		attachChild(mGuide);
		attachChild(mTitle);
		
		

		mTitleFadeOut= new AlphaModifier(0.2f, 1, 0, this);
		mGuideFadeOut= new AlphaModifier(0.2f, 1, 0, this);
		mTitle.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(5, this), mTitleFadeOut));
	}

	@Override
	public void onModifierStarted(IModifier<IEntity> modifier, IEntity item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onModifierFinished(IModifier<IEntity> modifier, IEntity item) {
		
		if(modifier == mTitleFadeOut)
		{
			mGuide.registerEntityModifier(new SequenceEntityModifier(new AlphaModifier(0.2f, 0, 1, this), new DelayModifier(5, this), mGuideFadeOut));
		}
		else if(modifier == mGuideFadeOut)
		{
			SceneManager.getInstance().showScene(new GameScene(Level.loadFromJson(Constants.MAIN_MENU_NAME)));
		}
		
		
	}
	
}
