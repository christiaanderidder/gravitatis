package nl.awesomestuff.gravitatis.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nl.awesomestuff.gravitatis.DBOpenHelper;
import nl.awesomestuff.gravitatis.Highscore;
import nl.awesomestuff.gravitatis.Level;

import org.andengine.audio.music.MusicFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

public class ResourceManager extends Object
{
	private static ResourceManager mInstance;
	
	private Engine mEngine;
	private Context mContext;
	private Camera mCamera;
	private PhysicsWorld mPhysicsWorld;
	private String mPreviousAssetBasePath = "";
	private TextureCollection mTextures;
	private MusicCollection mMusic;
	private SoundCollection mSounds;
	private FontCollection mFonts;
	private Map<String, Level> mLevels;
	private DBOpenHelper mDb;
	
	private ResourceManager()
	{
		mTextures = new TextureCollection();
		mMusic = new MusicCollection();
		mSounds = new SoundCollection();
		mFonts = new FontCollection();
		mLevels = new HashMap<String, Level>();
	}
	
	public static ResourceManager getInstance()
	{
		if(mInstance == null)
			mInstance = new ResourceManager();
			
		return mInstance;
	}

	public void setup(Engine engine, Context context, Camera camera, PhysicsWorld physicsWorld)
	{
		mEngine = engine;
		mContext = context;
		mCamera = camera;
		mPhysicsWorld = physicsWorld;
		mDb = new DBOpenHelper(mContext);
	}
	
	public ArrayList<Highscore> getHighScoresForLevel(String levelName)
	{
		return mDb.getHighScoresForLevel(levelName);
	}
	
	public Highscore getHighScoreForLevel(String levelName)
	{
		return mDb.getHighScoreForLevel(levelName);
	}
	
	public void addHighScore(Highscore h)
	{
		mDb.addHighScore(h);
	}
	
	public void loadResources()
	{
		loadTextures();
		loadMusic();
		loadFonts();

	}
	
	public void unloadResources()
	{
		unloadTextures();
		unloadMusic();
		unloadFonts();
	}

	/*
	 * Textures
	 */
	private void loadTextures()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("textures/");

		//BuildableBitmapTextureAtlas repAtlas = new BuildableBitmapTextureAtlas(mEngine.getTextureManager(), 80, 80, TextureOptions.REPEATING_BILINEAR);
		//mTextures.background = BitmapTextureAtlasTextureRegionFactory.createFromAsset(repAtlas, mContext, "blueprint.png");
		
		BuildableBitmapTextureAtlas regAtlas = new BuildableBitmapTextureAtlas(mEngine.getTextureManager(), 1024, 1024);
		BuildableBitmapTextureAtlas splashAtlas = new BuildableBitmapTextureAtlas(mEngine.getTextureManager(), 2048, 2048);
		mTextures.player = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "player.png");
		mTextures.scoreOrb = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "score-orb.png");
		mTextures.gravityModifier = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "gravity-modifier.png");
		mTextures.gravityArrow = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "gravityarrow.png");
		mTextures.inventory = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "inventory.png");
		mTextures.wall = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "wall.png");
		mTextures.portal = BitmapTextureAtlasTextureRegionFactory.createFromAsset(regAtlas, mContext, "portal.png");
		
		mTextures.menu = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashAtlas, mContext, "menu.png");
		mTextures.title = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashAtlas, mContext, "title.png");
		mTextures.guide = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashAtlas, mContext, "splash.png");
		try
		{
			//repAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			//repAtlas.load();
			splashAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			splashAtlas.load();
			
			regAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			regAtlas.load();
		}
		catch (TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}

		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(mPreviousAssetBasePath);
	}
	
	private void unloadTextures()
	{
		mTextures.player.getTexture().unload();
		mTextures.player.getTexture().unload();
		mTextures.scoreOrb.getTexture().unload();
		mTextures.gravityModifier.getTexture().unload();
		mTextures.gravityArrow.getTexture().unload();
		mTextures.inventory.getTexture().unload();
		mTextures.wall.getTexture().unload();
		mTextures.menu.getTexture().unload();
	}
	
	/*
	 * Sounds
	 */
	private void loadMusic()
	{
		MusicFactory.setAssetBasePath("sounds/music/");
		try
		{
			mMusic.menu = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), mContext, "menu.mp3");
		}
		catch (IOException e)
		{
			Log.v("Sounds Load", "Exception:" + e.getMessage());
		}
	}
	
	private void unloadMusic()
	{
		if(!mMusic.menu.isReleased()) mMusic.menu.release();
	}

	/*
	 * Fonts
	 */
	private void loadFonts()
	{
		FontFactory.setAssetBasePath("fonts/");
		
		mFonts.text = FontFactory.create(mEngine.getFontManager(), mEngine.getTextureManager(), 512, 256, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 64f, true, Color.WHITE_ABGR_PACKED_INT);
		mFonts.textSmall = FontFactory.create(mEngine.getFontManager(), mEngine.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 32f, true, Color.WHITE_ABGR_PACKED_INT);
		mFonts.textSmall.load();
		mFonts.text.load();
	}


	private void unloadFonts()
	{
		mFonts.text.unload();
		mFonts.textSmall.unload();
	}
	
	/*
	 * Getters
	 */
	
	public Map<String, Level> getLevels()
	{
		return mLevels;
	}
	
	public TextureCollection getTextures()
	{
		return mTextures;
	}
	
	public SoundCollection getSounds()
	{
		return mSounds;
	}

	public MusicCollection getMusic()
	{
		return mMusic;
	}

	public FontCollection getFonts()
	{
		return mFonts;
	}
	
	public PhysicsWorld getPhysicsWorld()
	{
		return mPhysicsWorld;
	}

	public float getCameraWidth()
	{
		return mCamera.getWidth();
	}

	public float getCameraHeight()
	{
		return mCamera.getHeight();
	}

	public float getCameraScaleFactorX()
	{
		return mCamera.getWidth();
	}

	public float getCameraScaleFactorY()
	{
		return mCamera.getHeight();
	}

	public Engine getEngine()
	{
		return mEngine;
	}
	
	public Camera getCamera()
	{
		return mCamera;
	}

	public Context getContext()
	{
		return mContext;
	}

	public void setPhysicsWorld(PhysicsWorld physicsWorld) {
		mPhysicsWorld = physicsWorld;
		
	}
}