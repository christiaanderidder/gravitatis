package nl.awesomestuff.gravitatis.resources;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;

public class TextureCollection
{
	public ITextureRegion player;
	public ITextureRegion scoreOrb;
	public ITextureRegion gravityArrow;
	public ITextureRegion gravityModifier;
	public ITextureRegion inventory;
	public ITextureRegion wall;
	public ITextureRegion menu;
	public ITextureRegion portal;
	public TextureRegion title;
	public TextureRegion guide;
}
