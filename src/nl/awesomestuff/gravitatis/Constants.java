package nl.awesomestuff.gravitatis;

import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.util.adt.color.Color;

import android.hardware.SensorManager;

public class Constants
{
	public static final String LEVEL_FIXED 				= "fixed";
	public static final String LEVEL_PLACED 			= "placed";
	public static final String LEVEL_INVENTORY 			= "inventory";
	public static final String LEVEL_OBJECT_TYPE 		= "type";
	public static final String LEVEL_OBJECT_X 			= "x";
	public static final String LEVEL_OBJECT_Y 			= "y";
	public static final String LEVEL_OBJECT_WIDTH		= "width";
	public static final String LEVEL_OBJECT_HEIGHT		= "height";
	public static final String LEVEL_OBJECT_ROTATION	= "rotation";
	
	public static final Color LEVEL_BACKGROUND_COLOR	= new Color(0.251f, 0.482f, 0.725f, 1.0f);
	public static final Color LEVEL_LAUNCH_PAD_COLOR	= new Color(1, 1, 1, 0.3f);
	public static final Color LEVEL_GRID_LINE_COLOR		= new Color(1, 1, 1, 0.3f);
	public static final Color COLOR_RED					= new Color(1, 0, 0, 1);
	public static final Color COLOR_WHITE				= new Color(1, 1, 1, 1);
	public static final int LEVEL_LAUNCH_PAD_RADIUS		= 200;
	public static final float LEVEL_PLAYER_RESET_SPEED	= 1f;
	public static final int LEVEL_GRID_CELL_SIZE		= 32;
	
	public static final int SIDEBAR_WIDTH	= 128;
	
	public static final float PHYSICS_GRAVITY			= SensorManager.GRAVITY_EARTH;
	public static final int PHYSICS_STEPS_SECOND 		= 60;
	public static final int PHYSICS_VELOCITY_ITERATIONS = 8;
	public static final int PHYSICS_POSITION_ITERATIONS = 3;
	
	public static final float PHYSICS_PPM				= PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
	public static final String LEVEL_NAME = "name";
	public static final String MAIN_MENU_NAME = "menu";
	
	public static final int SCORE_INCREASE_ORB = 1000;
	public static final int SCORE_INCREASE_LEVEL = 1000;
	public static final int SCORE_DECREASE_SEC = 100;
	public static final float LEVEL_LAUNCH_PAD_POWER = 50;
	
}
