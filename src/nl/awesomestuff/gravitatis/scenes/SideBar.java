package nl.awesomestuff.gravitatis.scenes;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;


public class SideBar extends Rectangle
{
	public SideBar(float width, float height, VertexBufferObjectManager vbom)
	{
		super(0, 0, width, height, vbom);
		setColor(new Color(0,0,0));
		
		
	}
}
