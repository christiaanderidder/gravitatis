package nl.awesomestuff.gravitatis.scenes;

import nl.awesomestuff.gravitatis.resources.ResourceManager;

import org.andengine.engine.Engine;
import org.andengine.entity.scene.Scene;

public class SceneManager
{
	private static SceneManager INSTANCE;
	
	private SceneManager()
	{
		mEngine = ResourceManager.getInstance().getEngine();
	}

	public static SceneManager getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new SceneManager();
		}
		return INSTANCE;
	}

	private ManagedScene mCurrentScene;
	private ManagedScene mNextScene;
	private Engine mEngine;

	public void onBackPressed()
	{
		if(mCurrentScene != null)
		{
			mCurrentScene.onBackPressed();
		}
	}
	
	public void onDevModeToggled(boolean debugMode)
	{
		if(mCurrentScene != null)
		{
			mCurrentScene.onDevModeToggled(debugMode);
		}
	}
	
	public void showScene(ManagedScene scene)
	{
		mNextScene = scene;
		
		// Reset the camera.
		mEngine.getCamera().set(0f, 0f, ResourceManager.getInstance().getCameraWidth(), ResourceManager.getInstance().getCameraHeight());
		
		// If the new managed scene does not have a loading screen.
		// Set pManagedScene to mNextScene and apply the new scene to the engine.
		mEngine.setScene(mNextScene);
		
		// If a previous managed scene exists, hide and unload it.
		if(mCurrentScene != null)
		{
			mCurrentScene.onUnload();
		}
		
		// Load and show the new managed scene, and set it as the current scene.
		mNextScene.onLoad();

		mCurrentScene = mNextScene;
	}
	
	public Scene getCurrentScene()
	{
		return mCurrentScene;
	}
}