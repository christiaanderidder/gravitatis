package nl.awesomestuff.gravitatis.scenes;

import org.andengine.entity.scene.Scene;

public abstract class ManagedScene extends Scene
{
	private boolean mCleanup;
	
	public ManagedScene()
	{
		mCleanup = false;
	}
	
	public void onLoad()
	{
		
	}
	
	public void onUnload(){

	}
	public void onBackPressed(){
		
	}
	public void onDevModeToggled(boolean debugMode){
		
	}
}
