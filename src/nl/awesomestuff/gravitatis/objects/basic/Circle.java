package nl.awesomestuff.gravitatis.objects.basic;

import org.andengine.entity.primitive.DrawMode;
import org.andengine.entity.primitive.Mesh;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Circle extends Mesh {

	static final int VERTEX_COUNT = 50;

	public Circle(float x, float y, float radius, final VertexBufferObjectManager vbom)
	{
		this(x, y, buildVertices(x, y, radius), vbom);
	}
	
	private Circle(float x, float y, float[] vertices, final VertexBufferObjectManager vbom)
	{
		super(x, y, vertices, vertices.length / VERTEX_SIZE, DrawMode.TRIANGLE_FAN, vbom, DrawType.STATIC);
	}

	private static float[] buildVertices(float x, float y, float radius) {

		float[] vertices = new float[VERTEX_COUNT * Mesh.VERTEX_SIZE];
		
		double rad = 0;
		
		for(int i = 0; i < VERTEX_COUNT; i++)
		{
			rad = Math.toRadians(i * Mesh.VERTEX_SIZE * 360 / (VERTEX_COUNT * Mesh.VERTEX_SIZE));
			vertices[(i * Mesh.VERTEX_SIZE) + Mesh.VERTEX_INDEX_X] = (float)Math.cos(rad) * radius;
			vertices[(i * Mesh.VERTEX_SIZE) + Mesh.VERTEX_INDEX_Y] = (float)Math.sin(rad) * radius;
		}
				
		return vertices;
	}
}