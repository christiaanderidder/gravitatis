package nl.awesomestuff.gravitatis.objects;

import nl.awesomestuff.gravitatis.Constants;
import nl.awesomestuff.gravitatis.ILaunchListener;
import nl.awesomestuff.gravitatis.objects.basic.Circle;

import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class LaunchPad extends GameObject
{
	private GameObject mPlayer;
	private float[] mCircleCenter;
	private float mRadius;
	private float mMaxPower;
	private boolean mHasLaunched;
	private ILaunchListener mLaunchListener;
	
	public LaunchPad(float x, float y, GameObject player, ILaunchListener launchListener, VertexBufferObjectManager vbo)
	{
		super(x, y);
		mMaxPower = Constants.LEVEL_LAUNCH_PAD_POWER;
		mLaunchListener = launchListener;
		mRadius = Constants.LEVEL_LAUNCH_PAD_RADIUS;
		mHasLaunched = false;
		setSize(mRadius * 2, mRadius * 2);
		Circle c = new Circle(0, 0, mRadius, vbo);
		c.setColor(new Color(1.f,1.f,1.f));
		c.setPosition(getWidth()/2.f, getHeight() /2.f);
		c.setAlpha(0.3f);
		setDrawable(c);
	
		mPlayer = player;
		
		mCircleCenter = convertLocalCoordinatesToParentCoordinates(0.f,0.f);
		
		resetPlayer();
		mPlayer.setVisible(true);
	}

	@Override
    public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{	
		final float relativeX = localX - mRadius;
		final float relativeY = localY - mRadius;
		
		final float distanceFromCenter = (float)Math.sqrt(Math.pow(relativeX, 2) + Math.pow(relativeY, 2));
		final float clampedX = (distanceFromCenter > mRadius) ? ((relativeX / distanceFromCenter) * mRadius) + mRadius : localX;
		final float clampedY = (distanceFromCenter > mRadius) ? ((relativeY / distanceFromCenter) * mRadius) + mRadius : localY;
		float[] parentPosition = { 0, 0 };
		if(!mHasLaunched)
		{
			switch (event.getAction())
			{	
			 	case TouchEvent.ACTION_DOWN:
				case TouchEvent.ACTION_MOVE:				
						parentPosition = convertLocalCoordinatesToParentCoordinates(clampedX, clampedY);
						mPlayer.getBody().setTransform(parentPosition[0] / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, parentPosition[1] / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, mPlayer.getBody().getAngle());
					break;
				case TouchEvent.ACTION_UP:
				case TouchEvent.ACTION_CANCEL:					
						if(distanceFromCenter > 0.25f * mRadius)
						{
							mHasLaunched = true;
							getDrawable().setVisible(false);
							mPlayer.getBody().setType(BodyType.DynamicBody);
							
							mPlayer.getBody().applyLinearImpulse(((clampedX - mRadius) / mRadius) * -mMaxPower, ((clampedY - mRadius) / mRadius) * -mMaxPower, 0, 0);
							mLaunchListener.onLaunched();
						}
						else
						{
							resetPlayer();
						}
					break;
			}
		}
		return true;
	}
	
	public void resetPlayer()
	{
		mCircleCenter = convertLocalCoordinatesToParentCoordinates(mRadius,mRadius);
		mHasLaunched = false;
		getDrawable().setVisible(true);
		getDrawable().setAlpha(0.3f);
		mPlayer.getBody().setLinearVelocity(0, 0);
		mPlayer.getBody().setAngularVelocity(0);
		mPlayer.getBody().setType(BodyType.StaticBody);
		mPlayer.getBody().setTransform(mCircleCenter[0] / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, mCircleCenter[1] / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, mPlayer.getBody().getAngle());
	}
	
	public boolean hasLaunched()
	{
		return mHasLaunched;
	}
}
