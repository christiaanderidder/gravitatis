package nl.awesomestuff.gravitatis.objects;

import nl.awesomestuff.gravitatis.resources.ResourceManager;
import nl.awesomestuff.gravitatis.resources.TextureCollection;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class ObjectFactory
{	
	public static GameObject create(String type, float x, float y, float width, float height, int rotation)
	{
		VertexBufferObjectManager vbom = ResourceManager.getInstance().getEngine().getVertexBufferObjectManager();
		PhysicsWorld physicsWorld = ResourceManager.getInstance().getPhysicsWorld();
		TextureCollection textures = ResourceManager.getInstance().getTextures();
		FixtureDef defaultFixture = PhysicsFactory.createFixtureDef(0, 0.1f, 0.1f);
		FixtureDef playerFixture = PhysicsFactory.createFixtureDef(0, 0.5f, 0.1f); 
		FixtureDef sensorFixture = PhysicsFactory.createFixtureDef(0, 0, 0, true);
		
		GameObject go = new GameObject(x, y);
		go.setRotation(rotation);
		go.setType(type);
		
		if(type.contains("loader-"))
		{
			ITextureRegion tex = textures.portal;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.setBody(PhysicsFactory.createBoxBody(physicsWorld, go, BodyType.StaticBody, sensorFixture));
		}
		else if(type.equals("exit"))
		{
			ITextureRegion tex = textures.portal;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.setBody(PhysicsFactory.createBoxBody(physicsWorld, go, BodyType.StaticBody, sensorFixture));
		}
		else if(type.equals("wall"))
		{
			ITextureRegion tex = textures.wall;
			go.setDrawable(new Sprite(width / 2.f, height / 2.f, width, height, tex, vbom));
			//go.getDrawable().setColor(new Color(0.55f, 0.32f, 0.11f));
			go.setSize(width, height);
			//go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, width, height, tex, vbom));
			go.setBody(PhysicsFactory.createBoxBody(physicsWorld, go, BodyType.StaticBody, defaultFixture));
			
		}
		else if(type.equals("score_orb"))
		{
			ITextureRegion tex = textures.scoreOrb;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.setBody(PhysicsFactory.createCircleBody(physicsWorld, go, BodyType.StaticBody, sensorFixture));
			
		}
		else if(type.equals("player"))
		{
			ITextureRegion tex = textures.player;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.setBody(PhysicsFactory.createCircleBody(physicsWorld, go, BodyType.StaticBody, playerFixture));
			
		}
		else if(type.equals("gravity_modifier"))
		{
			ITextureRegion tex = textures.gravityModifier;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.setBody(PhysicsFactory.createCircleBody(physicsWorld, go, BodyType.StaticBody, sensorFixture));
		}
		else if(type.equals("gravity_arrow"))
		{
			ITextureRegion tex = textures.gravityArrow;
			go.setSize(tex.getWidth(), tex.getHeight());
			go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, vbom));
			go.getDrawable().setAlpha(0.3f);
		}
		else
		{
			go.setSize(width, height);
			go.setDrawable(new Rectangle(width / 2.f, height / 2.f, width, height, vbom));
			go.setBody(PhysicsFactory.createBoxBody(physicsWorld, go, BodyType.StaticBody, defaultFixture));
			go.getDrawable().setColor(Color.ABGR_PACKED_RED_CLEAR);
		}
		
		
		//go.setSize(go.getWidth() + EditableGameObject.HANDLE_SIZE, go.getHeight()  + EditableGameObject.HANDLE_SIZE);
		//go.getDrawable().setPosition(go.getWidth() / 2.f, go.getHeight() / 2.f);
		
		
		return go;					
	}
	
	/*public EditableGameObject createDraggable(String type, float x, float y, float width, float height, int rotation)
	{
		//EditableGameObject go = new EditableGameObject(x, y, mVbo);
		
		//populate(go, type, width, height, rotation);
		//go.initialize();
		return null;		
	}
	
	private GameObject populate(GameObject go, String type, float width, float height, int rotation)
	{
		
	}
	
	public GameObject createPlayer(float x, float y)
	{
		GameObject go = new GameObject(x, y);
		go.setType("player");
		
		ITextureRegion tex = mTextures.player;
		go.setSize(tex.getWidth(), tex.getHeight());
		go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, mVbo));
		go.setBody(PhysicsFactory.createCircleBody(mPhysicsWorld, go, BodyType.StaticBody, mPlayerFixtureDef));
		mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(go, go.getBody(), true, true));
		
		return go;	
	}
	
	public GameObject createGravityArrow(float x, float y)
	{
		GameObject go = new GameObject(x, y);
		go.setType("gravity_arrow");
		
		ITextureRegion tex = mTextures.gravityArrow;
		go.setSize(tex.getWidth(), tex.getHeight());
		go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, mVbo));
		go.getDrawable().setAlpha(0.3f);
		go.setRotation(180);
		return go;	
		
	}
	
	public GameObject createGuiComponent(float x, float y, ITextureRegion tex)
	{
		GameObject go = new GameObject(x, y);
		go.setType("gui");
		go.setSize(tex.getWidth(), tex.getHeight());
		go.setDrawable(new Sprite(tex.getWidth() / 2.f, tex.getHeight() / 2.f, tex.getWidth(), tex.getHeight(), tex, mVbo));
		return go;	
		
	}*/
}
