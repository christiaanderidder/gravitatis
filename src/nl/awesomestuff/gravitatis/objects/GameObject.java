package nl.awesomestuff.gravitatis.objects;

import nl.awesomestuff.gravitatis.IGameObjectTouchListener;

import org.andengine.entity.Entity;
import org.andengine.entity.shape.IShape;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.input.touch.TouchEvent;

import com.badlogic.gdx.physics.box2d.Body;

public class GameObject extends Entity
{
	protected static final int DRAWABLE_Z_INDEX = 5;
	private IShape mDrawable;
	private Body mBody;
	private String mType;
	private IGameObjectTouchListener mTouchListener;
	private PhysicsConnector mPhysicsConnector;
	
	public GameObject(float x, float y)
	{
		super(x, y);	
	}
	
	public Body getBody()
	{
		return mBody;
	}
	
	public void setBody(Body body)
	{
		mBody = body;
		mBody.setUserData(this);
		mPhysicsConnector = new PhysicsConnector(this, mBody, true, true);
	}
	
	public PhysicsConnector getPhysicsConnector()
	{
		return mPhysicsConnector;
	}
	
	public IShape getDrawable()
	{
		return mDrawable;
	}
	
	public void setDrawable(IShape drawable)
	{
		if(mDrawable != null)
			detachChild(mDrawable);
		
		mDrawable = drawable;
		
		mDrawable.setAlpha(1.f);
		mDrawable.setZIndex(DRAWABLE_Z_INDEX);
		
		attachChild(mDrawable);
		
		sortChildren(true);
	}
	
	public String getType()
	{
		return mType;
	}
	
	public void setType(String type)
	{
		mType = type;
	}
	
	public void setTouchListener(IGameObjectTouchListener touchListener)
	{
		mTouchListener = touchListener;
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{
		if(mTouchListener != null)
			return mTouchListener.onGameObjectTouched(this, event, localX, localY);
		else
			return false;
	}
}
