package nl.awesomestuff.gravitatis.objects;

import org.andengine.input.touch.TouchEvent;

public interface EditableGameObjectTouchListener
{
	boolean onEditableGameObjectTouched(TouchEvent event, EditableGameObject go, float relativeX, float relativeY);
}
