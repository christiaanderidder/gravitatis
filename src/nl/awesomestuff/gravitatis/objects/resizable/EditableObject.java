package nl.awesomestuff.gravitatis.objects.resizable;

import nl.awesomestuff.gravitatis.Helpers;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

public class EditableObject extends Entity
{
	private Rectangle mRect;
	private float mInitialWidth;
	private float mInitialHeight;
	private float mInitialDistanceX;
	private float mInitialDistanceY;
	private float mInitialOffsetX;
	private float mInitialOffsetY;
	
	private Rectangle mActiveHandle;
	private Rectangle mResizeHandle;
	private Rectangle mRotateHandle;
	
	public static final int HANDLE_SIZE = 64;
	
	public EditableObject(VertexBufferObjectManager vbom)
	{
		setSize(300 + HANDLE_SIZE, 300 + HANDLE_SIZE);
		
		mRect = new Rectangle(getWidth()/2.f, getHeight()/2.f, 300, 300, vbom);
		attachChild(mRect);
		
		mResizeHandle = new Rectangle(0, 0, 64, 64, vbom);
		mResizeHandle.setColor(new Color(1.f,0,0));
		attachChild(mResizeHandle);
		
		mRotateHandle = new Rectangle(0, 0, 64, 64, vbom);
		mRotateHandle.setColor(new Color(1.f,0,0));
		attachChild(mRotateHandle);
		
		adjustHandles();
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{
		switch(event.getAction())
		{
			case TouchEvent.ACTION_DOWN:

				mInitialDistanceX = Math.abs(localX - (getWidth() / 2.f));
				mInitialDistanceY = Math.abs(localY - (getHeight() / 2.f));
				mInitialWidth = mRect.getWidth();
				mInitialHeight = mRect.getHeight();
				mInitialOffsetX = event.getX() - getX();
				mInitialOffsetY = event.getY() - getY();
				
				break;
			case TouchEvent.ACTION_MOVE:	
				
				checkActiveHandle(mResizeHandle, localX, localY);
				checkActiveHandle(mRotateHandle, localX, localY);

				if(mActiveHandle != null)
				{
					final float deltaX = (localX - (getWidth() / 2.f)) - mInitialDistanceX;
					final float deltaY = (localY - (getHeight() / 2.f)) - mInitialDistanceY;
					
					final float relativeX = event.getX() - getX();
					final float relativeY = event.getY() - getY();
					
					final float degrees = Helpers.positionToDegrees(relativeX, relativeY);
					
					if(mActiveHandle == mRotateHandle)
					{
						setRotation(degrees-90.f);
					}
					else
					{
						setSize(mInitialWidth + deltaX + HANDLE_SIZE, mInitialHeight + deltaY + HANDLE_SIZE);
						mRect.setPosition(getWidth()/2.f,getHeight()/2.f);
						mRect.setSize(mInitialWidth + deltaX, mInitialHeight + deltaY);
					}
					
					adjustHandles();
				}
				else
				{
					this.setPosition(event.getX() - mInitialOffsetX, event.getY() - mInitialOffsetY);
				}

				break;
			case TouchEvent.ACTION_UP:
			case TouchEvent.ACTION_CANCEL:
				mActiveHandle = null;
				break;
		}
		return true;
	}
	
	private void checkActiveHandle(Rectangle handle, float localX, float localY)
	{
		if(localX >= handle.getX()-(handle.getWidth() / 2.f) &&
		   localX <= handle.getX()+(handle.getWidth() / 2.f) &&
		   localY >= handle.getY()-(handle.getHeight() / 2.f) &&
		   localY <= handle.getY()+(handle.getHeight() / 2.f))
		{
			mActiveHandle = handle;
		}
	}
	
	private void adjustHandles()
	{
		final float rightX = mRect.getX() + (mRect.getWidth() / 2.f);
		final float bottomY = mRect.getY() - (mRect.getHeight() / 2.f);
		final float topY = mRect.getY() + (mRect.getHeight() / 2.f);
		
		mResizeHandle.setPosition(rightX, topY);
		mRotateHandle.setPosition(rightX, (mRect.getHeight()/2.f)+bottomY);
	}
}
