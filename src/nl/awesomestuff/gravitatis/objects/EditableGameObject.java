package nl.awesomestuff.gravitatis.objects;

import nl.awesomestuff.gravitatis.Constants;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

public class EditableGameObject extends GameObject
{
	public Rectangle mDropTarget;
	private VertexBufferObjectManager mVbom;
	
	private boolean mIsResizable;
	private boolean mIsRotatable;
	private boolean mIsActive;
	
	private float mInitialWidth;
	private float mInitialHeight;
	private float mInitialDistanceX;
	private float mInitialDistanceY;
	private float mInitialOffsetX;
	private float mInitialOffsetY;
	
	private Rectangle mActiveHandle;
	private Rectangle mResizeHandle;
	private Rectangle mRotateHandle;
	
	public static final int HANDLE_SIZE = 64;
	
	private EditableGameObjectTouchListener mTouchListener;
	
	public EditableGameObject(float x, float y, VertexBufferObjectManager vbom)
	{
		super(x, y);
		
		mVbom = vbom;
		
		mIsResizable = true;
		mIsRotatable = true;
	}
	
	public void setTouchListener(EditableGameObjectTouchListener touchListener)
	{
		mTouchListener = touchListener;
	}
	
	public void initialize()
	{
		
		
		
		mResizeHandle = new Rectangle(0, 0, HANDLE_SIZE, HANDLE_SIZE, mVbom);
		mResizeHandle.setColor(new Color(1.f,0,0));
		attachChild(mResizeHandle);
		
		mRotateHandle = new Rectangle(0, 0, HANDLE_SIZE, HANDLE_SIZE, mVbom);
		mRotateHandle.setColor(new Color(1.f,0,0));
		attachChild(mRotateHandle);
		
		setActive(false);
		
		adjustHandles();
		
		mDropTarget = new Rectangle(getWidth() / 2.f, getHeight() / 2.f, getWidth(), getHeight(), mVbom);
		mDropTarget.setColor(new Color(1, 0, 0, 0.3f));
		mDropTarget.setVisible(false);
		mDropTarget.setZIndex(GameObject.DRAWABLE_Z_INDEX - 2);
		
		attachChild(mDropTarget);	
	}

	/*@Override
    public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{
		if(mTouchListener == null)
			return false;

		return mTouchListener.onEditableGameObjectTouched(event, this, localX, localY);
	}*/
	
	@Override
	public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{
		switch(event.getAction())
		{
			case TouchEvent.ACTION_DOWN:

				mInitialDistanceX = Math.abs(localX - (getWidth() / 2.f));
				mInitialDistanceY = Math.abs(localY - (getHeight() / 2.f));
				mInitialWidth = getDrawable().getWidth();
				mInitialHeight = getDrawable().getHeight();
				mInitialOffsetX = event.getX() - getX();
				mInitialOffsetY = event.getY() - getY();
				break;
			case TouchEvent.ACTION_MOVE:	
				
				if(mIsResizable) checkActiveHandle(mResizeHandle, localX, localY);
				if(mIsRotatable) checkActiveHandle(mRotateHandle, localX, localY);

				if(mActiveHandle != null)
				{
					final float deltaX = (localX - (getWidth() / 2.f)) - mInitialDistanceX;
					final float deltaY = (localY - (getHeight() / 2.f)) - mInitialDistanceY;
					
					final float relativeX = event.getX() - getX();
					final float relativeY = event.getY() - getY();
					
					final float angle = (float)Math.atan2(relativeX, relativeY);//Helpers.positionToDegrees(relativeX, relativeY);
					
					if(mActiveHandle == mRotateHandle)
					{
						getBody().setTransform(getBody().getPosition(), (float) (angle - Math.PI) * -1);
					}
					else
					{
						setSize(mInitialWidth + deltaX + HANDLE_SIZE, mInitialHeight + deltaY + HANDLE_SIZE);
						getBody().setTransform(getBody().getPosition(), getBody().getAngle());
						getDrawable().setSize(mInitialWidth + deltaX, mInitialHeight + deltaY);
						
						//getBody().getTransform().setSize(mInitialWidth + deltaX, mInitialHeight + deltaY);
					}
					
					adjustHandles();
				}
				else
				{
					getBody().setTransform((event.getX() - mInitialOffsetX) / Constants.PHYSICS_PPM, (event.getY() - mInitialOffsetY) / Constants.PHYSICS_PPM, getBody().getAngle());
				}

				break;
			case TouchEvent.ACTION_UP:
			case TouchEvent.ACTION_CANCEL:
				if(mActiveHandle == mResizeHandle)
				{
					
				}
				mActiveHandle = null;
				break;
		}
		return mTouchListener.onEditableGameObjectTouched(event, this, localX, localY);
	}
	
	private void checkActiveHandle(Rectangle handle, float localX, float localY)
	{
		if(localX >= handle.getX()-(handle.getWidth() / 2.f) &&
		   localX <= handle.getX()+(handle.getWidth() / 2.f) &&
		   localY >= handle.getY()-(handle.getHeight() / 2.f) &&
		   localY <= handle.getY()+(handle.getHeight() / 2.f))
		{
			mActiveHandle = handle;
		}
	}
	
	private void adjustHandles()
	{
		final float rightX = getDrawable().getX() + (getDrawable().getWidth() / 2.f);
		final float bottomY = getDrawable().getY() - (getDrawable().getHeight() / 2.f);
		final float topY = getDrawable().getY() + (getDrawable().getHeight() / 2.f);
		
		mResizeHandle.setPosition(rightX, topY);
		mRotateHandle.setPosition(rightX, (getDrawable().getHeight()/2.f)+bottomY);
	}
	
	public void setActive(boolean active)
	{
		mIsActive = active;
		mResizeHandle.setVisible(mIsActive && mIsResizable);
		mRotateHandle.setVisible(mIsActive && mIsRotatable);
	}
	
	public void setResizable(boolean resizable)
	{
		mIsResizable = resizable;
	}
	
	public boolean getResizable()
	{
		return mIsResizable;
	}
	
	public void setRotatable(boolean rotatable)
	{
		mIsRotatable = rotatable;
	}
	
	public boolean getRotatable()
	{
		return mIsRotatable;
	}
}
