package nl.awesomestuff.gravitatis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import nl.awesomestuff.gravitatis.objects.GameObject;
import nl.awesomestuff.gravitatis.objects.ObjectFactory;
import nl.awesomestuff.gravitatis.resources.ResourceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

public class Level
{
	private ArrayList<GameObject> mFixedObjects;
	private ArrayList<GameObject> mPlacedObjects;
	private ArrayList<GameObject> mInventoryObjects;
	private String mName;
	private String mFileName;
	
	public Level()
	{
		mFixedObjects = new ArrayList<GameObject>();
		mPlacedObjects = new ArrayList<GameObject>();
		mInventoryObjects = new ArrayList<GameObject>();
	}
	
	public void setName(String name)
	{
		mName = name;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public String getFileName()
	{
		return mFileName;
	}
	
	public void setFileName(String fileName)
	{
		mFileName = fileName;
	}
	
	
	
	public ArrayList<GameObject> getFixedObjects()
	{
		return mFixedObjects;
	}
	
	public ArrayList<GameObject> getInventoryObjects()
	{
		return mInventoryObjects;
	}
	
	public ArrayList<GameObject> getPlacedObjects()
	{
		return mPlacedObjects;
	}
	
	public void takeFromInventory(GameObject go)
	{
		if(!mPlacedObjects.contains(go))
			mPlacedObjects.add(go);
		
		if(mInventoryObjects.contains(go))
			mInventoryObjects.remove(go);
		
		//Log.i("GRAVITATIS", "Take from inventory. placed: " + mPlacedObjects.size() + " inventory: " + mInventoryObjects.size());
	}
	
	public void addToInventory(GameObject go)
	{
		if(!mInventoryObjects.contains(go))
			mInventoryObjects.add(go);
		
		if(mPlacedObjects.contains(go))
			mPlacedObjects.remove(go);
		
		//Log.i("GRAVITATIS", "Add to inventory. placed: " + mPlacedObjects.size() + " inventory: " + mInventoryObjects.size());
	}
	
	public static Level loadFromJson(String fileName)
	{
		Level level = new Level();
		level.setFileName(fileName);
		try
        {
			String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/gravitatis/" + fileName + ".json";
			AssetManager assetManager = ResourceManager.getInstance().getContext().getAssets();
			File file = new File(path);
			
			InputStream inputStream;
			
			if(!file.exists())
				inputStream = assetManager.open("levels/" + fileName + ".json");
			else
				inputStream = new FileInputStream(file);
            
			Log.i("Gravitatis", "Reading " + path);
			
        	int size = inputStream.available();
            byte[] buffer = new byte[size];
            
            // Get the contents as a string
            inputStream.read(buffer);
            String jsonSource = new String(buffer, "UTF-8");

            //Log.i("Gravitatis", jsonSource);

            // Parse the Json
            JSONObject jsonRoot = new JSONObject(jsonSource);
            
    		level.setName(jsonRoot.getString(Constants.LEVEL_NAME));
            JSONArray jsonFixed = jsonRoot.getJSONArray(Constants.LEVEL_FIXED);
            JSONArray jsonInventory = jsonRoot.getJSONArray(Constants.LEVEL_INVENTORY);
            JSONArray jsonPlaced = jsonRoot.getJSONArray(Constants.LEVEL_PLACED);
            
            level.mInventoryObjects = readObjects(jsonInventory);
            level.mFixedObjects = readObjects(jsonFixed);
            level.mPlacedObjects = readObjects(jsonPlaced);

            inputStream.close();
            
            Log.i("Gravitatis", "Finished Reading " + path);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		return level;
	}
	
	private static ArrayList<GameObject> readObjects(JSONArray arr)
	{
		ArrayList<GameObject> objects = new ArrayList<GameObject>();
		
		// Create the GameObjects from the Json
        for (int i = 0; i < arr.length(); i++)
        {
			try
			{
				JSONObject jsonGameObject = arr.getJSONObject(i);
				
				String type 	= jsonGameObject.getString(Constants.LEVEL_OBJECT_TYPE);
	            float x 		= jsonGameObject.getLong(Constants.LEVEL_OBJECT_X);
	            float y 		= jsonGameObject.getLong(Constants.LEVEL_OBJECT_Y);
	            float width		= jsonGameObject.getLong(Constants.LEVEL_OBJECT_WIDTH);
	            float height	= jsonGameObject.getLong(Constants.LEVEL_OBJECT_HEIGHT);
	            int rotation 	= Math.abs(jsonGameObject.getInt(Constants.LEVEL_OBJECT_ROTATION));
	            //Log.i("ROTATION", type + " " +rotation);
	            objects.add(ObjectFactory.create(type, x, y, width, height, rotation));
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        return objects;
	}
	
	public static void saveToJson(Level level)
	{
		try
        {
			String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/gravitatis";
			File dir = new File(dirPath);
			if(!dir.exists())
			{
				dir.mkdirs();
			}
			
			String path = dirPath + "/" + level.getFileName() + ".json";
			File file = new File(path);
			if(!file.exists())
			{
				file.createNewFile();
			}
			
			Log.i("Gravitatis", "Writing " + path);
			
			FileOutputStream outputStream = new FileOutputStream(file);
            
            JSONObject jsonRoot = new JSONObject();
            jsonRoot.put(Constants.LEVEL_NAME, level.getName()); 
            jsonRoot.put(Constants.LEVEL_FIXED, writeObjects(level.getFixedObjects()));
            jsonRoot.put(Constants.LEVEL_INVENTORY, writeObjects(level.getInventoryObjects()));
            jsonRoot.put(Constants.LEVEL_PLACED, writeObjects(level.getPlacedObjects()));
            
            String jsonSource = jsonRoot.toString();
            
            //Log.i("Gravitatis", jsonSource);
            
            
			outputStream.write(jsonSource.getBytes("UTF-8"));
			outputStream.flush();
           
            
			outputStream.close();
			
			//Log.i("Gravitatis", "Finished Writing " + path);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private static JSONArray writeObjects(ArrayList<GameObject> editableObjects)
	{
		ArrayList<GameObject> objects = new ArrayList<GameObject>();
		JSONArray arr = new JSONArray();

		try
		{
			
			
			for (GameObject go : editableObjects)
            {
                JSONObject jsonGameObject = new JSONObject();
                
                jsonGameObject.put(Constants.LEVEL_OBJECT_TYPE, go.getType());
                jsonGameObject.put(Constants.LEVEL_OBJECT_X, go.getX());
                jsonGameObject.put(Constants.LEVEL_OBJECT_Y, go.getY());
                jsonGameObject.put(Constants.LEVEL_OBJECT_WIDTH, go.getWidth());
                jsonGameObject.put(Constants.LEVEL_OBJECT_HEIGHT, go.getHeight());
                jsonGameObject.put(Constants.LEVEL_OBJECT_ROTATION, Math.abs(go.getRotation()));
                arr.put(jsonGameObject);
            }
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return arr;
	}
}
