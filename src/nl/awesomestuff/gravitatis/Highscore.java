package nl.awesomestuff.gravitatis;


public class Highscore
{
	private long mId;
	private String mLevel;
	private long mScore;
	private long mTimestamp;
	
	private int mOrbCount;
	private int mOrbCollectedCount;

	
	public Highscore(String levelName)
	{
		mLevel = levelName;
	}
	
	public long getId() { return mId; }
	public void setId(long id) { mId = id; }
	
	public String getLevel() { return mLevel; }
	public void setLevel(String level) { mLevel = level; }
	
	public long getScore() { return mScore; }
	public void setScore(long score) { mScore = score; }
	
	public long getDate() { return mId; }
	public void setDate(long id) { mId = id; }
	
	public long getTimestamp() { return mTimestamp; }
	public void setTimestamp(long timestamp) { mTimestamp = timestamp; }
	
	public void increaseScore(long score) { mScore += score; }
	public void decreaseScore(long score) { mScore -= score; }
	public void addOrb() { mOrbCount++; }
	public void addOrbCollected() { mOrbCollectedCount++; }
	
	public void reset()
	{

		mTimestamp = System.currentTimeMillis();
		mOrbCollectedCount = 0;
	}

	public int getOrbCount() {
		return mOrbCount;
	}

	public int getOrbCollectedCount() {
		return mOrbCollectedCount;
	}
	
}
