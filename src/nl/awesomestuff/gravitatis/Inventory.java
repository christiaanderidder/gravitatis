package nl.awesomestuff.gravitatis;

import nl.awesomestuff.gravitatis.objects.GameObject;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


public class Inventory extends Sprite implements IGameObjectTouchListener
{
	private float mPreviousScrollPosition;
	private float mMinTopScrollPosition;
	private float mMaxBottomScrollPosition;
	
	private float mDeltaScroll;
	
	private VertexBufferObjectManager mVbom;
	private float mInitialX;
	private Level mLevel;
	private boolean mIsActive;

	
	public Inventory(Level level, ITextureRegion background, float x, float y, float width, float height, VertexBufferObjectManager vbom)
	{
		super(x, y, width, height, background, vbom);
		setAlpha(1.f);
		mIsActive = true;
		mMinTopScrollPosition = height;
		mMaxBottomScrollPosition = 0;
		mVbom = vbom;
		mLevel = level;
		
		recalculatePositions();
	}
	
	public void addObject(GameObject go)
	{
		go.getDrawable().setColor(Constants.COLOR_WHITE);
		go.getBody().setTransform(-100,-100, go.getBody().getAngle());
		mLevel.addToInventory(go);
		go.setTouchListener(this);
		recalculatePositions();
		
		
	}
	
	private void recalculatePositions()
	{
		float height = 0;		
		float lastY = 0;
		
		for(GameObject go : mLevel.getInventoryObjects())
		{
			lastY += (go.getHeight() / 2f);
			go.getBody().setTransform((getX()+8.f) / Constants.PHYSICS_PPM, (lastY) / Constants.PHYSICS_PPM, go.getBody().getAngle());			
			lastY += (go.getHeight() / 2f);
			height += go.getHeight();
			go.getDrawable().setScale(0.75f, 0.75f);
		}
		
		if(height > mMinTopScrollPosition)
		{
			setHeight(height);
		}
		else
		{
			setHeight(mMinTopScrollPosition);
		}	
		
		setPosition(getX(), (getHeight()/2.f));
	}
	
	public void setActive(boolean active)
	{
		mIsActive = active;
		for(GameObject go : mLevel.getInventoryObjects())
		{
			go.getDrawable().setVisible(active);
		}
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent event, float localX, float localY)
	{
		/*if(event.isActionDown())
		{
			mPreviousScrollPosition = event.getY();
			
	    }
	    if(event.isActionMove())
	    {
	    	final float newY = getY() + (event.getY() - mPreviousScrollPosition);
			final float topY = newY + (getHeight() / 2.f);
	    	final float bottomY = newY - (getHeight() / 2.f);
	    	
	    	final float delta = getY() - newY;
	    	
	    	if(topY >= mMinTopScrollPosition && bottomY <= mMaxBottomScrollPosition)
	    	{
	            setPosition(getX(), mDeltaScroll);
	            mPreviousScrollPosition = event.getY();
	    	}
	    	else if(topY < mMinTopScrollPosition)
	    	{
	    		setPosition(getX(), mMinTopScrollPosition - (getHeight() / 2.f));
	    	}
	    	else if(bottomY > mMaxBottomScrollPosition)
	    	{
	    		setPosition(getX(), mMaxBottomScrollPosition + (getHeight() / 2.f));
	    	}
	    	
	    	
	    	for(GameObject go : mLevel.getInventoryObjects())
			{
	    		go.getBody().setTransform(getX() / Constants.PHYSICS_PPM, (go.getY() - delta) / Constants.PHYSICS_PPM, go.getBody().getAngle());
			}
	    }*/
	    
		return false;
	}

	@Override
	public boolean onGameObjectTouched(GameObject go, TouchEvent event,	float relativeX, float relativeY)
	{
		if(mIsActive)
		{
			if(event.isActionDown())
			{
				mInitialX = event.getX();
			}
			else if(event.isActionMove())
			{
				final float deltaX = mInitialX - event.getX();
				
				if(deltaX > getWidth()/2.f)
				{
					
					//go.getBody().setTransform(event.getX(),  go.getY(), go.getBody().getAngle());
					//ResourceManager.getInstance().getPhysicsWorld().registerPhysicsConnector(go.getPhysicsConnector());
					//detachChild(go);
					//getParent().attachChild(go);
					mLevel.takeFromInventory(go);
					
					recalculatePositions();
					
					go.setTouchListener((IGameObjectTouchListener)getParent());
					
					go.getDrawable().setScale(1f,1f);
					
					
				}
			}

		}
		return true;
	}
}
